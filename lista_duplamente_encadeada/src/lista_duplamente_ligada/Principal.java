/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista_duplamente_ligada;

/**
 *
 * @author lab801
 */
public class Principal {

    public static void testarAdd() {
        List<Integer> lista = new List();
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        lista.add(5);
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        lista.add(5);
        lista.add(13);
        System.out.println("Lista: " + lista.toString());
        System.out.println("Tamanho: " + lista.getSize());
    }
    
    public static void testarInsert() {
        List<Integer> lista = new List();
        lista.insert(1,0);
        lista.insert(2,1);
        lista.insert(3,2);
        lista.insert(4,3);
        lista.insert(5,4);
        lista.insert(1,5);
        lista.insert(2,6);
        lista.insert(3,7);
        lista.insert(4,8);
        lista.insert(5,9);
        lista.insert(13,1);
        System.out.println("Lista: " + lista.toString());
        System.out.println("Tamanho: " + lista.getSize());
    }
    
    public static void testarRemove() {
        try {
            List<Integer> lista = new List();
            lista.insert(1,0);
            lista.insert(2,1);
            lista.insert(3,2);
            lista.insert(4,3);
            lista.insert(5,4);
            lista.insert(1,5);
            lista.insert(2,6);
            lista.insert(3,7);
            lista.insert(4,8);
            lista.insert(5,9);
            lista.insert(13,1);
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Removido posição 1: " + lista.removeAt(1));
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Removido posição 3: " + lista.removeAt(3));
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Posição valor 5: " + lista.indexOf(5));
            System.out.println("Removido valor 5: " + lista.remove(5));
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Posição valor 1: " + lista.indexOf(1));
            System.out.println("Removido valor 1: " + lista.remove(1));
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Posição valor 4: " + lista.indexOf(4));
            System.out.println("Removido valor 4: " + lista.remove(4));
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Posição valor 5: " + lista.indexOf(5));
            System.out.println("Removido valor 5: " + lista.remove(5));
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Removido posição 0: " + lista.removeAt(0));
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Existe 0? " + (lista.exists(0) ? "Sim" : "Não"));
            System.out.println("Existe 1? " + (lista.exists(1) ? "Sim" : "Não"));
            System.out.println("Existe 2? " + (lista.exists(2) ? "Sim" : "Não"));
            System.out.println("Existe 3? " + (lista.exists(3) ? "Sim" : "Não"));
            System.out.println("Existe 4? " + (lista.exists(4) ? "Sim" : "Não"));
            System.out.println("Existe 5? " + (lista.exists(5) ? "Sim" : "Não"));
            System.out.println("Existe 13? " + (lista.exists(13) ? "Sim" : "Não"));
            System.out.println("Inserindo 20 em 0: " + lista.set(20, 0));
            System.out.println("Inserindo 21 em 1: " + lista.set(21, 1));
            System.out.println("Inserindo 22 em 2: " + lista.set(22, 2));
            System.out.println("Inserindo 23 em 3: " + lista.set(23, 3));
            System.out.println("Inserindo 24 em 4: " + lista.set(24, 4));
            System.out.println("Lista: " + lista.toString());
            System.out.println("Tamanho: " + lista.getSize());
            System.out.println("Existe 20? " + (lista.exists(20) ? "Sim" : "Não"));
            System.out.println("Existe 21? " + (lista.exists(21) ? "Sim" : "Não"));
            System.out.println("Existe 22? " + (lista.exists(22) ? "Sim" : "Não"));
            System.out.println("Existe 23? " + (lista.exists(23) ? "Sim" : "Não"));
            System.out.println("Existe 4? " + (lista.exists(4) ? "Sim" : "Não"));
            System.out.println("Existe 5? " + (lista.exists(5) ? "Sim" : "Não"));
            System.out.println("Existe 13? " + (lista.exists(13) ? "Sim" : "Não"));
            System.out.println("Removido posição 0: " + lista.removeAt(0));
            System.out.println("Removido posição 0: " + lista.removeAt(0));
            System.out.println("Removido posição 0: " + lista.removeAt(0));
            System.out.println("Removido posição 0: " + lista.removeAt(0));
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
        }
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // testarAdd();
        // testarInsert();
        testarRemove();
    }
    
}
