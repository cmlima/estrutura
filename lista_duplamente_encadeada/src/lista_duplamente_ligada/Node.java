/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista_duplamente_ligada;

/**
 *
 * @author lab801
 * @param <T>
 */
public class Node<T> {
    
    public T data;
    public Node next;
    public Node previous;

    public Node(T data) {
        this.data = data;
        this.next = this.previous = null;
    }

    @Override
    public String toString() {
        return this.data.toString();
    }
}
