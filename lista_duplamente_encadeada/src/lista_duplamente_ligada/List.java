/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista_duplamente_ligada;

/**
 *
 * @author lab801
 */
public class List<T> {
    
    private int size;
    private Node<T> first;
    private Node<T> last;
    
    
    List() {
        this.size = 0;
        this.first = this.last = null;
    }

    public int getSize() {
        return this.size;
    }
    
    public boolean isEmpty() {
        return this.size == 0;
    }
    
    public void add(T data) {
        Node<T> temp = new Node(data);
        if (this.isEmpty()) {
            this.first = this.last = temp;
        } else {
            this.last.next = temp;
            this.last = temp;
        }
        this.size++;
    }

    public boolean insert(T data, int position) throws Exception {
        if (position < 0 || position > this.size) {
            return false;
        }
        Node<T> temp = new Node(data);
        if (this.isEmpty()) {
            this.first = this.last = temp;
        } else if (position == 0) {
            this.first.previous = temp;
            temp.next = this.first;
            this.first = temp;
        } else if (position == this.size) {
            this.last.next = temp;
            temp.previous = this.last;
            this.last = temp;
        } else {
            Node<T> next = this.find(position);
            Node<T> previous = next.previous;
            previous.next = temp;
            next.previous = temp;
            temp.next = next;
            temp.previous = previous;
        }
        this.size++;
        return true;
    }
    
    public int indexOf(T data) {
        Node<T> node = this.first;
        int position = 0;
        while (node != null) {
            if (node.data == data) {
                return position;
            }
            node = node.next;
            position++;
        }
        return -1;
    }
    
    private Node<T> find(int position) throws Exception {
        if (position < 0 || position > this.size) {
            throw new Exception("Posição inválida");
        }
        Node<T> node = this.first;
        int counter = 0;
        while (node != null) {
            if (counter == position) {
                return node;
            }
            node = node.next;
            counter++;
        }
        return null;
    }

    public T get(int position) throws Exception {
        return this.find(position).data;
    }
    
    public boolean set(T data, int position) throws Exception {
        if (position < 0 || position >= this.size) {
            return false;
        }
        Node<T> node = this.find(position);
        node.data = data;
        return true;
    }
    
    public boolean exists(T data) {
        return this.indexOf(data) >= 0;
    }
    
    public T removeAt(int position) throws Exception {
        if (this.isEmpty()) {
            throw new Exception("Lista vazia");
        }
        if (position < 0 || position > this.size) {
            throw new Exception("Posição inválida");
        }
        Node<T> temp = this.find(position);
        T val = temp.data;
        if (this.size == 1) {
            this.first = this.last = null;
        } else if (position == 0) {
            this.first = this.first.next;
            this.first.previous = null;
        } else if (position == this.size - 1) {
            this.last = this.last.previous;
            this.last.next = null;
        } else {
            Node previous = temp.previous;
            Node next = temp.next;
            if (next != null) {
                next.previous = previous;        
            }
            if (previous != null) {
                previous.next = next;
            }
        }
        size--;
        return val;
    }
    
    public T remove(T value) throws Exception {
        return this.removeAt(this.indexOf(value));
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "";
        }
        var builder = new StringBuilder();
        Node<T> node = this.first;
        while (node != null) {
            builder.append(node.data).append(" ");
            node = node.next;
        }
        return builder.toString().trim();
    }
    
}
